<?php
ob_start(); ?>
Produit
<?php
$titre = ob_get_clean();
ob_start(); ?>

<div style='padding-top:5%'></div>
<div class="container">
    <div class="row" style='height:auto'>
        <div class="col col-12 col-md-7">
            <img class='img-fluid ' src="./image/<?= $produit->getImage() ?>" alt="" >
        </div>
        <div class="col my-5 col-12 col-md-5">
    <h1><?= $produit->getModele()?></h1>
    <p class='h2 bg-danger'><?= $produit->getPrixUnitaire()?>€</p>
    <p><?= $produit->getDescription()?></p>
    <?php if(isset($_SESSION['autorise']) && $_SESSION['autorise']=="OKClient"){ ?>

           
<a href='index.php?uc=produit&action=ajoutPanier&idAjouterPanier=<?=$produit->getId_produit()?>'
class='btn btn-danger'>Ajouter au <i class='fas fa-cart-plus fa-2x'></i></a>
<?php    }else{ ?>
    <form action="index.php?uc=client&action=formulaire" method="post">
      <input type="hidden" name="produitConnexion" value='<?=$produit->getId_produit()?>'>
    
     <input type="submit" class='btn btn-danger' value="Ajouter au panier">        
    </form>

  <?php  } ?>
    </div>
    </div>
</div>



    
<?php 
$content = ob_get_clean();
require("view/template.php");