<?php
ob_start(); ?>
espace Client
<?php
$titre = ob_get_clean();
 ob_start() ?>
<h1 class="padding-5 py-5 display-3 text-center">
    Espace client
</h1>
<?php 
// var_dump($commandes)
?>
<div class="container">
    <div class="row">
        <div class="col col-12 col-md-6">
            <div class="ficheClient p-5 mb-5">
                <h2 class='display-4     pb-5'><b><?= $client->getNom_client()." ".$client->getPrenom() ?></b></h2>
                <p class='h2'><?= $client->getEmail() ?><br>
                            client n° <?= $client->getId_client() ?><br>
                            
                            <?= $client->getAdresse().", ".$client->getVille(). " ".$client->getCode_postal() ?></p>
            </div>
        </div>
        <div class="col col-12 col-md-6">
            <div class=''>
               <?php foreach($commandes as $commande){ $date = $commande->dateFR($commande->getDate_commande()) ?>
                
            <?= $date ." : commande n°". $commande->getId_commande() ?> <a class='btn btn-warning ms-5 my-2' href="index.php?uc=commande&action=afficheCommande&IdCommande=<?= $commande->getId_commande() ?>">facture</a><br>
            <?php } ?> 
            </div>
            
        </div>
    </div>
</div>
<?php
 $content = ob_get_clean();
 require("view/template.php");