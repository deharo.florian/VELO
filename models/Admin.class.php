<?php
class Admin{
    private string $login ;
    private string $mdp ;
    

    /**
     * Get the value of login
     */ 
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Set the value of login
     *
     * @return  self
     */ 
    public function setLogin($newLogin)
    {
        if(strlen($newLogin)>=2){
        $this->login = $newLogin;

        }

        return $this;
    }

    /**
     * Get the value of mdp
     */ 
    public function getMdp($newMdp)
    {
        if(strlen($newMdp)>=4){
        return $this->mdp;
        }
    }

    /**
     * Set the value of mdp
     *
     * @return  self
     */ 
    public function setMdp($mdp)
    {
        $this->mdp = $mdp;

        return $this;
    }

    public static function verifier($email, $mdp){
        // $mdp=md5($mdp);
        $req = MonPDO::getInstance()->prepare("select * from admin where email =:email and mdp=:mdp");
        $req->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE,'admin');
        $req->bindParam('email',$email);
        $req->bindParam('mdp',$mdp);
        $req->execute();
        $leResultat=$req->fetchAll();
        $nb_lignes=count($leResultat);
        if($nb_lignes == 0){
            $rep=false;
        }else{
            $rep=true ;
        }
        return $rep ;
    }

    public static function deco(){
        if(isset($_SESSION["autorise"])){
        unset($_SESSION['autorise']);
        }
    }
}