<?php
ob_start(); ?>
accueil
<?php
$titre = ob_get_clean();
ob_start(); ?>



<!-- contenu > accueil -->
    <div class="container-fluid container_accueil">
        <div class="row">
            <div class="col offset-1 text-center text-sm-start">
                <h1 class='display-1 text-danger '>Velo Club</h1>
                <h2>Surpassez vos limites ! </h2>
            </div>
        </div>
    </div>

    <!-- description du site -->
    <div class="container-fluid grise py-5">
        <div class="row">
            <div class="col col-6 offset-3 col-md-4 offset-md-4 text-light text-center">
                <h2>HISTOIRE DU VELOCLUB</h2>
                <p >Lorem ipsum dolor sit amet consectetur adipisicing elit. Velit eaque quaerat illum 
                    voluptas inventore laboriosam ratione. Molestiae, quia distinctio consequuntur ullam facilis 
                    laboriosam quos, repellat, maiores ad consequatur fugit animi?</p>
            </div>
        </div>
    </div>

    <!-- carroussel categorie velo -->
    <div class="container-fluid rose">
        <div class="row py-5">
            <div class="col col-8 offset-2">
                <h3 class='display-3 text-center pb-5'><b>Les categories de vélo</b></h3>
                <div id="carouselExampleCaptions" class="carousel slide pb-5" data-bs-ride="carousel">
                    <div class="carousel-indicators">
                <!-- premier foreach  -->
                         <!-- premier foreach  --><?php  $i = 0 ; 
                        foreach($lesCategories as $categorie){
                            if($i==0){ ?>
                            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="<?= $categorie->getID_categorie() - 1 ?>" aria-label="Slide <?php $categorie->getID_categorie() ; ?>" class="active" aria-current="true" ></button>
                        
                            <?php }else{ ?>

                                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="<?= $categorie->getID_categorie() - 1 ?>" aria-label="Slide <?php $categorie->getID_categorie()  ?>"  ></button>

                        <?php  } $i++ ; 
                        } ?>
                   </div>
                    <div class="carousel-inner">
                       <!-- deuxieme foreach -->
                       <?php  $a = 0 ; 
                            foreach($lesCategories as $categorie){
                                if($a==0){  ?>
                                    <div class="carousel-item active">
                                        <a href=""><img src="./image/<?= $categorie->getImage_categorie();?>"   alt="..."></a>
                                        <div class="carousel-caption d-none d-sm-block">
                                            <h3><?= $categorie->getNom();?></h3>
                                        </div>
                                    </div>
                               <?php }else{ ?>

                                    <div class="carousel-item">
                                        <a href=""><img src="./image/<?= $categorie->getImage_categorie();?>"  class="d-block " alt="..."></a>
                                        <div class="carousel-caption d-none d-sm-block">
                                            <h3><?= $categorie->getNom();?></h3>
                                        </div>
                                    </div>

                            <?php  } $a++ ; 
                              } ?>
                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </button>
                 </div>
            </div>
        </div>
    </div>
    <!-- les cards avec les trois meilleurs produits -->
    <div class="container-fluid grise py-5">
        <h5  class='display-3 text-center pb-5 text-danger'>Vos produits préférés</h5>
        <div class="row justify-content-around">
            <div class="card p-0" style="width: 18rem;">
                <img src="./image/vtt.jpg" class="card-img-top" alt="...">
                <div class="card-body   text-center">
                    <h5 class="card-title">Nom produit</h5>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                   
                </div>
                <div class="card-img-overlay">
                    <h5 class="card-title">Prix</h5>
                </div>
            </div>
            <div class="card p-0" style="width: 18rem;">
                <img src="./image/vtt.jpg" class="card-img-top" alt="...">
                <div class="card-body   text-center">
                    <h5 class="card-title">Nom produit</h5>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    
                </div>
                <div class="card-img-overlay">
                    <h5 class="card-title">Prix</h5>
                </div>
            </div>
            <div class="card p-0" style="width: 18rem;">
                <img src="./image/vtt.jpg" class="card-img-top" alt="...">
                <div class="card-body  text-center">
                    <h5 class="card-title">Nom produit</h5>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
               
                </div>
                <div class="card-img-overlay">
                    <h5 class="card-title">Prix</h5>
                </div>
            </div>

        </div>
    </div>
    
<?php 
$content = ob_get_clean();
require("view/template.php");