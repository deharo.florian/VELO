<?php
ob_start(); ?>
Panier
<?php
$titre = ob_get_clean();
 ob_start(); ?>
<div class="container my-5" style='padding-top:10%;padding-bottom:15%' >
  <div class="row">
    <?php
if(!isset($_SESSION['panier']))
{
$_SESSION['panier'] =array(); // création de la variable de session
}?>


<table class='table' >
             <thead>
               <tr>
                 <th scope='col'>Produit</th>
                 <th scope='col'>Prix Unit</th>
                 <th scope='col'>Quantite</th>
                 <th scope='col'>Montant</th>
                 <th scope='col'>Ajouter </th>
                 <th scope='col'>Retirer </th>
               </tr>
<?php
// var_dump($_SESSION['panier']);
if(!empty($_SESSION['panier'])){
    // require "config.php";
    //         $bdd = connect();
            $prixHT = 0 ;
            $nombredarticle = 0 ; 
            foreach($_SESSION['panier'] as $id=>$quantite ){
            // var_dump($_SESSION['panier'][$id]);
                 
            $produit = Produit::TrouverUnProduit($id);
            
            // while ($produit = $resultat->fetch(PDO::FETCH_OBJ)){
              // var_dump($produit->getId_produit());
              // var_dump($resultat->fetch(PDO::FETCH_OBJ));
             ?>
             <tbody>
               <tr>
                 <th scope='row'><?= $produit->getModele() ?></th>
                 <td><?= $produit->getPrixUnitaire() ?> €</td>
                 <td><?= $quantite ?></td>
                 <td><?= $quantite * $produit->getPrixUnitaire()?> €</td>
                 <td><?= "<a href='index.php?uc=produit&action=ajoutPanier2&idAjouterPanier2=".$produit->getId_produit()."' class='btn btn-outline-success'>+</a>" ?></td>
                 <td><?= "<a href='index.php?uc=produit&action=retirerPanier&idRetirerPanier=".$produit->getId_produit()."' class='btn btn-outline-success'>-</a>" ?></td>
               </tr>
               <?php 
               
             $prixHT = $prixHT + $quantite * $produit->getPrixUnitaire() ;
             $nombredarticle = $nombredarticle + $quantite ; 
         }
         
    //  }
    //  var_dump($_SESSION['panier']);
     $_SESSION['badge'] = $nombredarticle ;

 } ?></table>
 </div>
 
<?php 
 if(!empty($_SESSION['panier'])){
   ?>
   <div class="row">
     <div class="col col-md-6 offset-md-6">
      <table class="table">
                  
                  <tbody>
                    <tr class="table-danger">
                      <td>Total HT</td>
                      <td><?= $prixHT ?> €</td>
                      
                    </tr>
                    <tr class="table-danger">
                      <td>TVA(19.6%)</td>
                      <td><?php echo round($prixHT*0.196,2)?> €</td>
                      
                    </tr>
                    <tr class="table-danger">
                      <td>Frais de port</td>
                      <td>5 €</td>
                      
                    </tr>
                    <tr class="table-danger">
                      <td>TOTAL TTC</td>
                      <td><?php echo round($prixHT*1.196,2)?> €</td>
                      
                    </tr>
                  </tbody>
                </table> 
                <div class="row">
                  <div class="col justify-content-between d-flex">
                <a class="btn btn-dark" href="index.php?uc=produit&action=viderPanier" role="button">Vider le panier</a>
                <a class="btn btn-dark" href="index.php?uc=commande&action=commander" role="button">Payer</a>
   

                  </div>

                </div>
              </div> 
  </div>

     <?php } ?>
  
</div>

      <?php $content = ob_get_clean();
require("view/template.php");
?>