<?php
session_start();

if (empty($_SESSION['token'])) {
    $_SESSION['token'] = bin2hex(random_bytes(32));
}
$token = $_SESSION['token'];

include("models/monPdo.php");
include("models/Produit.class.php");
include("models/Admin.class.php");
include("models/Categorie.class.php");
include("models/Client.class.php");
include("models/Commande.class.php");


    function securiser($donnees){
    //efface les espaces en debut et fin de chaine
    $donnees = trim($donnees);
    //enleve les antislashes pour evite d'ignorer du code
    $donnees = stripslashes($donnees);
    //empeche l'interpretation de balise HTML
    $donnees = htmlspecialchars($donnees);
    return $donnees ;
}





if(empty($_GET["uc"])){
    $uc="accueil";
}else{
    $uc= securiser($_GET["uc"]);
}

switch($uc){
    case "accueil" :
        $lesCategories=Categorie::afficherTousCategorie();
        include("view/accueil.php");
        break ;
    case "produit":
        include("controller/controllerProduit.php");
        break ;
    case "admin" :
        include("controller/controllerAdmin.php");
        break ;
    case "client" :
        include("controller/controllerClient.php");
        break ;
    case "panier" :
        include("controller/controllerProduit.php");
        break ;
    case  "commande" :
        include("controller/controllerCommande.php");
        break ;
}