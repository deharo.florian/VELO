<?php ob_start(); ?>
<div style='padding-top:5%'></div>
<div class="container">
             <h1>Formulaire de modification</h1>
             <form method='Post' action="index.php?uc=produit&action=traitementModif" enctype="multipart/form-data">
               
                
             <?= var_dump($produit) ?>
                <div class="form mb-3 ">
                  <label for="modele">Modele</label>
                   <input type="text" class="form-control" id="modele" placeholder="<?= $produit->getModele() ?>" name="modele">
                 </div>

                <div class="form mb-3">
                     <label for="description">Description</label>
                   <textarea class="form-control" placeholder="" id="description" name="description" style="height: 100px"><?= $produit->getDescription() ?></textarea>
                </div>

                <div class="form mb-3">
                   <label for="prixUnitaire">Prix</label>
                   <input type="text" class="form-control" id="prixUnitaire" placeholder="<?= $produit->getPrixUnitaire() ?>" name="prixUnitaire">
                </div>

                <div class="input-group mb-3">
                    <label class="input-group-text" for="estDisponible">disponibilité</label>
                    <select class="form-select" id="estDisponible" name="estDisponible">
                        <?php if($produit->getEstDisponible() == 1){?>
                        <option value="1" selected>Disponible</option>
                        <option value="0">indisponible</option>

                        <?php }else{ ?>     
                        <option value="1">Disponible</option>
                        <option value="0" selected>indisponible</option>
                            
                       <?php } ?>
                    </select>
                </div>    
                <div class="input-group mb-3">
                    <label class="input-group-text" for="enfant">Pour</label>
                    <select class="form-select" id="enfant" name="enfant">
                        <?php if($produit->getEnfant() == 1){ ?>
                        <option value="1" selected>enfant</option>
                        <option value="0">adulte</option>

                        <?php }else{ ?>
                            
                        <option value="1">enfant</option>
                        <option value="0" selected>adulte</option>

                       <?php } ?>
                    </select>
                </div> 
                <div class="input-group mb-3">
                    <label class="input-group-text" for="id_categorie">Categorie</label>
                    <select class="form-select" name="id_categorie" id="id_categorie">
                        <?php switch($produit->getId_categorie()){
                            case "1" : ?>
                                <option value="1" selected>BMX</option>
                                <option value="2" >Velo de course</option>
                                <option value="3">Velo de ville</option>
                                <option value="4">VTT</option>
                       <?php break ;
                        case "2" : ?>
                        <option value="1">BMX</option>
                        <option value="2" selected>Velo de course</option>
                        <option value="3">Velo de ville</option>
                        <option value="4">VTT</option>

                        <?php break ;
                         case "3" : ?>
                        <option value="1">BMX</option>
                        <option value="2">Velo de course</option>
                        <option value="3" selected>Velo de ville</option>
                        <option value="4">VTT</option>
                        <?php break ;
                         case "4" : ?>
                        <option value="1">BMX</option>
                        <option value="2">Velo de course</option>
                        <option value="3">Velo de ville</option>
                        <option value="4" selected>VTT</option>
                        <?php break ;
                    } ?>
                    </select>
                </div> 
                <div class="input-group mb-3">
                <select name="id_type" class="form-select">
                    <?php if($produit->getId_type() == 1){ ?>
                    <option value="1" selected >velo</option>
                    <option value="2">accessoire</option> 

                   <?php }else{?>
                    <option value="1">velo</option>
                    <option value="2" selected >accessoire</option> 

                   <?php }?>
                </select> 
                </div>   
                
                <div class="input-group mb-3">
                    <label class="form-label" for="customFile">Image du produit </label>
                    <input type="file" name="image" class="form-control" id="customFile" />
                </div>
                <input type="hidden" name="idModif" value="<?= $modifier ?>">
                <button class="btn btn-primary">
                   Modifier
                </button>
             </form>
         </div>
<?php $content = ob_get_clean();
require("view/template.php");
?>