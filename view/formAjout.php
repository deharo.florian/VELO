<?php ob_start(); ?>
<div style='padding-top:5%'></div>
<div class="container">
             <h1>Formulaire d'ajout</h1>
             <form method='Post' action="index.php?uc=produit&action=TraitementAjout" enctype="multipart/form-data">
               
                
                 
                <div class="form-floating mb-3 ">
                   <input type="text" class="form-control" id="modele" placeholder="" name="modele">
                   <label for="modele">modele</label>
                </div>

                <div class="form-floating mb-3">
                    <textarea class="form-control" placeholder="Leave a comment here" id="description" name="description" style="height: 100px"></textarea>
                    <label for="description">Description</label>
                </div>

                <div class="form-floating mb-3">
                   <input type="text" class="form-control" id="prixUnitaire" name="prixUnitaire">
                   <label for="prixUnitaire">Prix</label>
                </div>

                <div class="input-group mb-3">
                    <label class="input-group-text" for="estDisponible">disponibilité</label>
                    <select class="form-select" id="estDisponible" name="estDisponible">
                        <option value="1" selected>Disponible</option>
                        <option value="0">indisponible</option>
                    </select>
                </div>    
                <div class="input-group mb-3">
                    <label class="input-group-text" for="enfant">Pour</label>
                    <select class="form-select" id="enfant" name="enfant">
                        <option value="1" selected>enfant</option>
                        <option value="0">adulte</option>
                    </select>
                </div> 
                <div class="input-group mb-3">
                    <label class="input-group-text" for="id_categorie">Categorie</label>
                    <select class="form-select" name="id_categorie" id="id_categorie">
                        <option value="1" selected>BMX</option>
                        <option value="2">Velo de course</option>
                        <option value="3">Velo de ville</option>
                        <option value="4">VTT</option>
                    </select>
                </div> 
                <div class="input-group mb-3">
                <select name="id_type" class="form-select">
                    <option value="1">velo</option>
                    <option value="2">accessoire</option> 
                </select> 
                </div>   
                
                <div class="input-group mb-3">
                    <label class="form-label" for="customFile">Image du produit </label>
                    <input type="file" name="image" class="form-control" id="customFile" />
                </div>

                <button class="btn btn-primary">
                   Ajouter
                </button>
             </form>
         </div>
<?php $content = ob_get_clean();
require("view/template.php");
?>