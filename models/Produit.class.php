<?php
class Produit {
    public int $Id_produit ;
    public string $modele ;
    public string $description ;
    public int $prixUnitaire ;
    public bool $estDisponible  ;
    public bool $enfant ;
    public int $id_categorie ;
    public int $id_type ;
    public string $image ; 
    
    public function hydrate($modele,$description,$prixUnitaire,$estDisponible,$enfant,$id_categorie,$id_type,$image){
        $this->setModele($modele);
        $this->setDescription($description);
        $this->setPrixUnitaire($prixUnitaire);
        $this->setEstDisponible($estDisponible);
        $this->setEnfant($enfant);
        $this->setId_categorie($id_categorie);
        $this->setId_type($id_type);
        $this->setImage($image);
    }
    

    
    /**
     * Get the value of id_produit
     */ 
    public function getId_produit()
    {
        return $this->Id_produit;
    }

    /**
     * Set the value of id_produit
     *
     * @return  self
     */ 
    public function setId_produit($id_produit)
    {
        $this->Id_produit = $id_produit;

        return $this;
    }

    /**
     * Get the value of modele
     */ 
    public function getModele()
    {
        return $this->modele;
    }

    /**
     * Set the value of modele
     *
     * @return  self
     */ 
    public function setModele($modele)
    {
        $this->modele = $modele;

        return $this;
    }

    /**
     * Get the value of description
     */ 
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of description
     *
     * @return  self
     */ 
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of prixUnitaire
     */ 
    public function getPrixUnitaire()
    {
        return $this->prixUnitaire;
    }

    /**
     * Set the value of prixUnitaire
     *
     * @return  self
     */ 
    public function setPrixUnitaire($prixUnitaire)
    {
        $this->prixUnitaire = $prixUnitaire;

        return $this;
    }

    /**
     * Get the value of estDisponible
     */ 
    public function getEstDisponible()
    {
        return $this->estDisponible;
    }

    /**
     * Set the value of estDisponible
     *
     * @return  self
     */ 
    public function setEstDisponible($estDisponible)
    {
        $this->estDisponible = $estDisponible;

        return $this;
    }

    /**
     * Get the value of enfant
     */ 
    public function getEnfant()
    {
        return $this->enfant;
    }

    /**
     * Set the value of enfant
     *
     * @return  self
     */ 
    public function setEnfant($enfant)
    {
        $this->enfant = $enfant;

        return $this;
    }

    /**
     * Get the value of id_categorie
     */ 
    public function getId_categorie()
    {
        return $this->id_categorie;
    }

    /**
     * Set the value of id_categorie
     *
     * @return  self
     */ 
    public function setId_categorie($id_categorie)
    {
        $this->id_categorie = $id_categorie;

        return $this;
    }

    /**
     * Get the value of id_type
     */ 
    public function getId_type()
    {
        return $this->id_type;
    }

    /**
     * Set the value of id_type
     *
     * @return  self
     */ 
    public function setId_type($id_type)
    {
        $this->id_type = $id_type;

        return $this;
    }

    /**
     * Get the value of image
     */ 
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set the value of image
     *
     * @return  self
     */ 
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    public static function afficherTous()
    {
        $req=MonPdo::getInstance()->prepare("select * from produit inner join categorie on produit.id_categorie = categorie.Id_categorie order by Id_produit");
        $req->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'produit');
        $req->execute();
        $lesResultats=$req->fetchAll();
        return $lesResultats ;
    }

    public static function afficherProduitComplet($id)
    {
        $req=MonPdo::getInstance()->prepare("select * from produit inner join categorie on produit.id_categorie = categorie.Id_categorie where Id_produit=:id");
        $req->bindValue(':id', $id );
        $req->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'produit');
        $req->execute();
        $lesResultats=$req->fetch();
        return $lesResultats ;
    }

    public static function TrouverUnProduit($id_produit)
    {
        $req=MonPdo::getInstance()->prepare("select * from produit where Id_produit=:id_produit ");
        $req->bindValue(':id_produit', $id_produit );
        $req->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'produit');
        $req->execute();
        $lesResultats=$req->fetch();
        return $lesResultats ;
    }

    public static function afficherParNomEtParType($modele,$idType)
    {
        $req=MonPdo::getInstance()->prepare("select * from produit where modele like :modele and id_type like :idType");
        $req->bindValue(":modele","$modele%",PDO::PARAM_STR);
        $req->bindValue(":idType",$idType,PDO::PARAM_STR);
        $req->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'produit');
        $req->execute();
        $lesResultats=$req->fetchAll();
        return $lesResultats ;
    }
    public static function afficherParType($idType){
        $req=MonPdo::getInstance()->prepare("select * from produit where Id_type like :idType");
        $req->bindValue(":idType",$idType,PDO::PARAM_STR);
        $req->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'produit');
        $req->execute();
        $lesResultats=$req->fetchAll();
        return $lesResultats ;
        var_dump($lesResultats);
    }
    
    public static function ajouter(produit $produit)
        { 
            $req=MonPdo::getInstance()->prepare("insert into  produit(modele,description,prixUnitaire,estDisponible,enfant,id_categorie,id_type,image) value (:modele,:description ,:prixUnitaire,:estDisponible,:enfant,:id_categorie,:id_type,:image)");
            $req->bindValue(':modele', $produit->getModele() );
            $req->bindValue(':description', $produit->getDescription() );
            $req->bindValue(':prixUnitaire', $produit->getPrixUnitaire() );
            $req->bindValue(':estDisponible', $produit->getEstDisponible());
            $req->bindValue(':enfant', $produit->getEnfant() );
            $req->bindValue(':id_categorie', $produit->getId_categorie() );
            $req->bindValue(':id_type', $produit->getId_type() );
            $req->bindValue(':image', $produit->getImage() );
            $nb=$req->execute();
            return $nb ;
        }

    public static function modifier(produit $produit)
        {
            $req=MonPdo::getInstance()->prepare("update `produit` SET modele=:modele,description=:description,prixUnitaire=:prixUnitaire,estDisponible=:estDisponible,enfant=:enfant,id_categorie=:id_categorie,id_type=:id_type,image=:image WHERE Id_produit=:id_produit");
            $req->bindValue(':modele', $produit->getModele() );
            $req->bindValue(':description', $produit->getDescription() );
            $req->bindValue(':prixUnitaire', $produit->getPrixUnitaire() );
            $req->bindValue(':estDisponible', $produit->getEstDisponible());
            $req->bindValue(':enfant', $produit->getEnfant() );
            $req->bindValue(':id_categorie', $produit->getId_categorie() );
            $req->bindValue(':id_type', $produit->getId_type() );
            $req->bindValue(':image', $produit->getImage() );
            
            $req->bindValue(':id_produit', $produit->getId_produit() );
            $nb=$req->execute();
            return $nb ;
        }

        public static function supprimer($id_produit){
            $req=MonPDO::getInstance()->prepare("DELETE FROM `produit` WHERE Id_produit=:id_produit");
            $req->bindValue(':id_produit', $id_produit);
            $nb=$req->execute();
            return $nb ;
        }

        public static function afficherPanier($id){
            $req=MonPDO::getInstance()->prepare("select FROM `produit` WHERE Id_produit=:id");
            $req->bindValue(':id', $id);
            
            $nb=$req->fetchAll();
            return $nb ;
        }

        

        public static function ajouterPanier($idAjouterPanier){
    

            if(isset($idAjouterPanier)){
                // if(!isset($_SESSION['panier']))
                // {
                // $_SESSION['panier'] =array(); // création de la variable de session
                // }
                
                if(isset($_SESSION['panier'][$idAjouterPanier]))
                {
                $_SESSION['panier'][$idAjouterPanier]++ ; //ajoute 1 à la quantité
                }
                else
                {
                $_SESSION['panier'][$idAjouterPanier]=1 ; // sinon met un dans la quantité 
                }
    
            }
            $_SESSION['badge']++ ; 
            $_SESSION['succes']="le produit a été ajouté au panier !" ;
        }

        public static function ajouterPanier2($idAjouterPanier2){
            if(isset($_SESSION['panier'][$idAjouterPanier2]))
            {
                $_SESSION['panier'][$idAjouterPanier2]++ ; //ajoute 1 à la quantité
            }
        }
        public static function viderPanier(){
            unset($_SESSION['panier']);
            unset($_SESSION['badge']);
        }


        public static function retirerPanier($idRetirerPanier){
            if(isset($idRetirerPanier)){
                if($_SESSION['panier'][$idRetirerPanier]==1){
                    unset($_SESSION['panier'][$idRetirerPanier]);
                    unset($_SESSION['badge']);
                    
                }else{
                    $_SESSION['panier'][$idRetirerPanier]-- ;
            
                }
            }
            
        }
        // public static function afficherPanier($id,$quantite){
        //     $req=MonPdo::getInstance()->prepare("select * from produit where id_produit='$id=>$quantite'");
        //     $req->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'produit');
        //     $req->execute();
        //     $lesResultats=$req->fetchAll();
        //     return $lesResultats ;
        // }
}

