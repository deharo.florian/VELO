<?php ob_start(); ?>

<div style='padding-top:5%'></div>
<div class="container">
    <div class="row justify-content-center">
        <div class="col"><h2>le produit : <?= $recherche ?></h2></div>
        <form class="form-inline my-2 my-lg-0" method="POST" action="index.php?uc=produit&action=recherche">
                    <input class="form-control mr-sm-2" type="search" placeholder="Rechercher" aria-label="Search" name="recherche">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit"><i class="fas fa-search"></i></button>
                </form>
    </div>
    <div class="row">
        <?php
        if($lesProduits == null){ ?>

         <div class="col offset-3 col-6">  <img class="img-fluid"  src="./image/aucun_resultat.jpg" alt=""></div>
        <?php  }else{ 
       foreach($lesProduits as $produit){
            echo "<div class='card text-center' style='width: 15rem;'>
            <img class='card-img-top' src='./image/".$produit->getImage()."'>
            <div class='card-body'>
            <h5 class='card-title'>".$produit->getModele()."</h5>
            <p class='card-text'> ".$produit->getPrixUnitaire()."€</p>
            <a href='ajout_panier.php?ajout=".$produit->getId_produit()."'
            class='btn btn-danger'>Ajouter au <i class='fas fa-cart-plus fa-2x'></i></a>
            </div>
            </div>" ;
       }}
        ?>
    </div>
</div>

<?php $content = ob_get_clean();
require("view/template.php");
?>