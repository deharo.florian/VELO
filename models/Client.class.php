<?php
class Client{
    public int $Id_client;
    public string $Nom_client ;
    public string $prenom ;
    public string $email ;
    private string $mdp ;
    public string $adresse ;
    public string $ville ;
    public string $code_postal;
    

    /**
     * Get the value of login
     */ 
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of login
     *
     * @return  self
     */ 
    public function setEmail($newEmail)
    {
        if(strlen($newEmail)>=2){
        $this->email = $newEmail;

        }

        return $this;
    }

    /**
     * Get the value of mdp
     */ 
    public function getMdp($newMdp)
    {
        if(strlen($newMdp)>=4){
        return $this->mdp;
        }
    }

    /**
     * Get the value of Id_client
     */ 
    public function getId_client()
    {
        return $this->Id_client;
    }

    /**
     * Set the value of Id_client
     *
     * @return  self
     */ 
    public function setId_client($Id_client)
    {
        $this->Id_client = $Id_client;

        return $this;
    }

    /**
     * Get the value of Nom
     */ 
    public function getNom_client()
    {
        return $this->Nom_client;
    }

    /**
     * Set the value of Nom
     *
     * @return  self
     */ 
    public function setNom($Nom_client)
    {
        $this->Nom_client = $Nom_client;

        return $this;
    }

    /**
     * Get the value of prenom
     */ 
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set the value of prenom
     *
     * @return  self
     */ 
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get the value of adresse
     */ 
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set the value of adresse
     *
     * @return  self
     */ 
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get the value of ville
     */ 
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set the value of ville
     *
     * @return  self
     */ 
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get the value of code_postal
     */ 
    public function getCode_postal()
    {
        return $this->code_postal;
    }

    /**
     * Set the value of code_postal
     *
     * @return  self
     */ 
    public function setCode_postal($code_postal)
    {
        $this->code_postal = $code_postal;

        return $this;
    }

    /**
     * Set the value of mdp
     *
     * @return  self
     */ 
    public function setMdp($mdp)
    {
        $this->mdp = $mdp;

        return $this;
    }

    public static function verifier($email, $mdp){
        // $mdp=md5($mdp);
        $req = MonPDO::getInstance()->prepare("select * from client where email=:email and mdp=:mdp");
        $req->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE,'client');
        $req->bindParam('email',$email);
        $req->bindParam('mdp',$mdp);
        $req->execute();
        $leResultat=$req->fetchAll();
        $nb_lignes=count($leResultat);
        if($nb_lignes == 0){
            $rep=false;
        }else{
            $rep=true ;
        }
        return $rep ;
    }

    public static function deco(){
        if(isset($_SESSION["autorise"])){
        unset($_SESSION['autorise']);
        }
    }

    public static function afficherClient($email, $mdp){
    $req = MonPDO::getInstance()->prepare("select * from client where email=:email and mdp=:mdp");
    $req->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE,'client');
    $req->bindParam('email',$email);
    $req->bindParam('mdp',$mdp);
    $req->execute();
    $leResultat=$req->fetch();
    return $leResultat ;
    }

    public static function afficherClientParId($id){
        $req = MonPDO::getInstance()->prepare("select * from client where Id_client=:id");
    $req->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE,'client');
    $req->bindParam(':id',$id);
    $req->execute();
    $leRes=$req->fetch();
    return $leRes ;
    }

    public static function confInscription($email,$Nom_client,$prenom,$mdp,$confmdp,$adresse,$code_postal,$ville){
        $req = MonPDO::getInstance()->prepare("select * from client where email=:email");
        $req->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE,'client');
        $req->bindParam(':email',$email);
        $req->execute();
        $clientbdd=$req->rowCount();
        
       

        // (null`, `Nom_client`, `prenom`, `email`, `mdp`, `adresse`, `ville`, `code_postal`) '
            if($mdp == $confmdp){
                $mdp = md5($mdp);
                    if($clientbdd==0){
                        $req2 = MonPDO::getInstance()->prepare("insert into client values  (null,:Nom_client,:prenom,:email, :mdp, :adresse,:ville,:code_postal)");
                        $req2->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE,'client');
                        $req2->bindParam(':Nom_client',$Nom_client);
                        $req2->bindParam(':prenom',$prenom);
                        $req2->bindParam(':adresse',$adresse);
                        $req2->bindParam(':code_postal',$code_postal);
                        $req2->bindParam(':ville',$ville);
                        $req2->bindParam(':email',$email);
                        $req2->bindParam(':mdp',$mdp);
                        $req2->execute();


                        // $sql = "insert into client values (null,'$login_client', '$motdepasse', '$adresse',$code_postal,'$ville','$tel')";
                        // $result = $bdd->query($sql);    
                        header("location:index.php");
                    }else{
                        $_SESSION['erreur']="l'utlisateur a deja un compte ! ";
                        header("location:index.php?uc=client&action=inscription");
                    }
            }else{
                $_SESSION['erreur']="le mot de passe ne correspond pas ! ";
                header("location:index.php?uc=client&action=inscription");
            
        }
        // else{
        //     $_SESSION['erreur']="les caracteres du login sont incorrectes !";
        //     header("location:inscription.php");
        // }
    }
    

}