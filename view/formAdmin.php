<?php
ob_start(); ?>
espace Admin
<?php
$titre = ob_get_clean();
 ob_start(); ?>

<div style='padding-top:5%'></div>
<!-- <div class="container">
    <div class="row col-lg-3 col-md-4 col-sm-6 col-12">
        <h1>Formulaire Admin</h1>
        <form action="index.php?uc=admin&action=verif" method="post">
            <?php if(isset($_SESSION['erreur'])){ ?>
            <div class="alert alert-danger">
                <?= $_SESSION['erreur'] ;?>
            </div>

            <?php } ?>
            <div class="row mb-2">
            <label for="inputEmail"> Email* : </label>
            <input name='email' id="inputEmail" class="form-control" required type="mail">
            </div>
            <div class="row mb-2">
            <label for="inputMDP"> MDP* : </label>
            <input  name='mdp' id="inputMDP" class="form-control" required minlength="4" type="password">
        </div>
            <button class="btn btn-danger">Envoyer</button>

            
        </form>
    </div>
</div> -->

<div class="container-fluid">
    <div class='divform'>
        <div class='sousdivform row align-items-center'>
            <h1 class='text-center'>Espace <span class='badge rounded-pill bg-warning text-dark'>Admin</span></h1>
            <?php if(isset($_SESSION['erreur'])){ ?>
            <div class="alert alert-danger">
                <?= $_SESSION['erreur'] ;?>                         
            </div>

            <?php } ?>
                <div class="col col-12 col-md-6 formulaireDiv">
                    <form action="index.php?uc=admin&action=verif"  method="post">
                    <br>
                    <div class="form">
                    <label for="inputEmail"> Email* : </label>
            <input name='email' id="inputEmail" class="form-control" required type="mail">
           
                    </div>
                    <br>
                    <div class="form">
                        <label for="inputMDP"> MDP* : </label>
                        <input  name='mdp' id="inputMDP" class="form-control" required minlength="4" type="password">
                    </div>
                    <br>
                    <br>
                       <input type="submit" class='btn btn-primary' value="se connecter">
                    </form>
                </div>
                <div class="col col-12 offset-md-1 col-md-5">
                    <img style='width:100%' src="image\5243.jpg" alt="">
                </div>
        </div>
    </div>
</div>
<?php $content = ob_get_clean();
require("view/template.php");
?>