<?php
ob_start(); ?>
Espace Admin
<?php
$titre = ob_get_clean();
 ob_start(); ?>


<div style='padding-top:5%'></div>


<div class="container">
    <div class="row">
<a href='index.php?uc=produit&action=formAjout'
     class='btn btn-danger'>Ajouter Produit</a>
 <?php foreach($lesProduits as $produit){ ?>
        
   <div class='card text-center' style='width: 15rem;'>
    <h2>id = <?= $produit->getId_produit() ?></h2>
     <img class='card-img-top' alt='<?= $produit->getImage() ?>' src='./image/<?= $produit->getImage() ?>'>
     <div class='card-body'>
     <p class='card-text'>modele : <?= $produit->getModele() ?></p>
     <p class='card-text'>prix : <?= $produit->getPrixUnitaire() ?>€</p>
     <p class='card-text'><?php $dispo = $produit->getEstDisponible() == 1 ?  "disponible" :  "indisponible" ; echo $dispo ; ?></p>
     <p class='card-text'><?php $enfant = $produit->getEnfant() == 1 ?  "pour enfant" :  "pour adulte" ; echo $enfant ; ?></p>
     <p class='card-text'>categorie : <?= $produit->nom ?></p>
     <a href='index.php?uc=produit&action=modifier&modifier=<?= $produit->getId_produit()  ?>'
     class='btn btn-primary'>Modifier Produit</a>
     <br>
     <a href='index.php?uc=produit&action=supprimer&idSuppr=<?= $produit->getId_produit()  ?>'
     class='btn btn-danger'>Supprimer Produit</a>     
     </div>
     </div>
  <?php } ?>
</div>
</div>
<?php
 $content = ob_get_clean();
 require("view/template.php");