<?php
ob_start(); ?>
Produits
<?php
$titre = ob_get_clean();
 ob_start(); ?>

<div class="container-fluid container_accueil_velo">
        <div class="row">
            <div class="col offset-1 text-center text-sm-start">
                <h1 class='display-1 text-danger '>Velo Club</h1>
                
                <form class="form-inline my-2 my-lg-0" method="POST" action="index.php?uc=produit&action=recherche">
                    <input class="form-control mr-sm-2" type="search" placeholder="Rechercher" aria-label="Search" name="recherche">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit"><i class="fas fa-search"></i></button>
                </form>
            </div>
        </div>
    </div>
<div class="container">
    <div class="row text-center py-5">
        <div class="col bg-danger"><h2>Nos produits</h2></div>
        
    </div>
    <div class="row d-flex justify-content-center justify-content-sm-start">
        <?php
        foreach($lesProduits as $produit){ ?>
        <!-- <div class="col"> -->
           <div class='card text-center mb-2' style='width: 15rem;'>
            <img class='card-img-top' src='./image/<?= $produit->getImage() ?>'>
            <div class='card-body'>
            <h5 class='card-title'><?= $produit->getModele() ?></h5>
            <p class='card-text'><?= $produit->getPrixUnitaire()?>€</p>
            <a href="index.php?uc=produit&action=fiche&id=<?=$produit->getId_produit()?>">plus d'info</a>
            <?php if(isset($_SESSION['autorise']) && $_SESSION['autorise']=="OKClient"){ ?>

           
            <a href='index.php?uc=produit&action=ajoutPanier&idAjouterPanier=<?=$produit->getId_produit()?>'
            class='btn btn-danger'>Ajouter au <i class='fas fa-cart-plus fa-2x'></i></a>
            <?php    }else{ ?>
                <form action="index.php?uc=client&action=formulaire" method="post">
                  <input type="hidden" name="produitConnexion" value='<?=$produit->getId_produit()?>'>
                
                 <input type="submit" class='btn btn-danger' value="Ajouter au panier">        
                </form>

              <?php  } ?>

            </div>
           </div>
        <!-- </div> -->
     <?php 
       }
        ?>
    </div>
</div>
<?php $content = ob_get_clean();
require("view/template.php");
?>