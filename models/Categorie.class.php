<?php
class Categorie {
    public int $Id_categorie ;
    public string $nom ;
    public string $image_categorie ; 

    

    /**
     * Get the value of Id_categorie
     */ 
    public function getId_categorie()
    {
        return $this->Id_categorie;
    }

    /**
     * Set the value of Id_categorie
     *
     * @return  self
     */ 
    public function setId_categorie($Id_categorie)
    {
        $this->Id_categorie = $Id_categorie;

        return $this;
    }

    /**
     * Get the value of nom
     */ 
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set the value of nom
     *
     * @return  self
     */ 
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    
    /**
     * Get the value of image_categorie
     */ 
    public function getImage_categorie()
    {
        return $this->image_categorie;
    }

    /**
     * Set the value of image_categorie
     *
     * @return  self
     */ 
    public function setImage_categorie($image_categorie)
    {
        $this->image_categorie = $image_categorie;

        return $this;
    }

  
    
    public static function afficherTousCategorie()
    {
        $req=MonPdo::getInstance()->prepare("select * from categorie");
        $req->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'categorie');
        $req->execute();
        $lesResultats=$req->fetchAll();
        return $lesResultats ;
    }

}