<?php
    class Commande{

    public int $Id_commande ;
    public string $date_commande  ;
    public ?string $date_livraison ; 
    public string $etat ;
    public int $id_client ;

    

    /**
     * Get the value of Id_commande
     */ 
    public function getId_commande()
    {
        return $this->Id_commande;
    }

    /**
     * Set the value of Id_commande
     *
     * @return  self
     */ 
    public function setId_commande($Id_commande)
    {
        $this->Id_commande = $Id_commande;

        return $this;
    }

    /**
     * Get the value of date_commande
     */ 
    public function getDate_commande()
    {
        return $this->date_commande;
    }

    /**
     * Set the value of date_commande
     *
     * @return  self
     */ 
    public function setDate_commande($date_commande)
    {
        $this->date_commande = $date_commande;

        return $this;
    }

    /**
     * Get the value of date_livraison
     */ 
    public function getDate_livraison()
    {
        return $this->date_livraison;
    }

    /**
     * Set the value of date_livraison
     *
     * @return  self
     */ 
    public function setDate_livraison($date_livraison)
    {
        $this->date_livraison = $date_livraison;

        return $this;
    }

    /**
     * Get the value of etat
     */ 
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set the value of etat
     *
     * @return  self
     */ 
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get the value of id_client
     */ 
    public function getId_client()
    {
        return $this->id_client;
    }

    /**
     * Set the value of id_client
     *
     * @return  self
     */ 
    public function setId_client($id_client)
    {
        $this->id_client = $id_client;

        return $this;
    }

    public static function ajouterCommande($idClient,$panier)
        { 
             // ajoute la commande du client a la colonne commande
            $req=MonPdo::getInstance()->prepare("INSERT INTO `commande`(`Id_commande`, `date_commande`, `date_livraison`, `etat`, `id_client`) VALUES (null,'".date('Y-m-d')."',null,'En cours',:id_client)");
            $req->bindValue(':id_client',$idClient );
            $req->execute();

        //   ajouter 1 au nombre de commande pour la deuxieme requete
            $sqlcommande = MonPdo::getInstance()->prepare("select * from commande order by Id_client desc limit 1");
            $sqlcommande->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'commande');
            $sqlcommande->execute();  
            $commando = $sqlcommande->fetch();
            $commande= $commando->getId_commande() ;

            // creer un foreach pour la session panier avec l'id produit et la quantite par produit
            foreach($panier as $clef => $prod){
                
                // ajouter dans la table commande_produit les produits par commande
                $sql2 = MonPdo::getInstance()->prepare("INSERT INTO `commande_produit`(`Id_commande`, `Id_produit`, `quantite`) VALUES ($commande,$clef, $prod)");
                $resultatfinal = $sql2->execute();  
    
// echo $produit->id;
        }
        return $resultatfinal ;
        }
        
    
        public static function afficherDerniereCommande(){
            
            $sqlcommande = MonPdo::getInstance()->prepare("select * from commande order by Id_commande desc limit 1");
            $sqlcommande->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'commande');
            $sqlcommande->execute();
            $commandes = $sqlcommande->fetch();
            return $commandes ;
        }
        
        public static function  afficherCommandeParClient($id){
            $req =  MonPdo::getInstance()->prepare("select * from commande where id_client=:id");
            $req->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE,'commande');
            $req->bindParam(':id',$id);
            $req->execute();
            $comm = $req->fetchAll();
            return $comm ;
        }

        public static function afficherCommandeParId($id){
            $req =  MonPdo::getInstance()->prepare("select * from commande where id_commande=:id");
            $req->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE,'commande');
            $req->bindParam(':id',$id);
            $req->execute();
            $commande = $req->fetch();
            return $commande ;
        }

        // afficherDernierProduit(){
        //     $req =  MonPdo::getInstance()->prepare("SELECT * FROM produit INNER join commande_produit on produit.Id_produit = commande_produit.Id_produit where id_commande=:id");
        //     $req->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE,'produit');
        //     $req->bindParam(':id',$id);
        //     $req->execute();
        //     $commandeProduit = $req->fetchAll();
        //     return $commandeProduit ;
        // }

        public static function afficherProduitparCommande($id){
            $req =  MonPdo::getInstance()->prepare("SELECT * FROM produit INNER join commande_produit on produit.Id_produit = commande_produit.Id_produit where id_commande=:id");
            $req->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE,'produit');
            $req->bindParam(':id',$id);
            $req->execute();
            $commandeProduit = $req->fetchAll();
            return $commandeProduit ;

        }
        // retourne les date en francais a mettre dans l'index comme securite()
        public static function dateFR($date){
           $date = explode('-', $date, 3);
            $datefr = $date[2]."/".$date[1]."/".$date[0] ;
            return $datefr;
          }
    }
