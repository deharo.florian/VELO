<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title><?= $titre ?></title>

    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="stylesheet" href="style/style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

</head>

<body>
<!-- navbar -->
<?php if(!isset($_SESSION["autorise"])){ ?>
  <nav class="d-flex grise fixed-top  justify-content-center text-center text-lg-start justify-content-lg-end  navbar navbar-expand-lg navbar-dark">

<div >
    
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="index.php">Accueil</a>
        </li>
        <?php if(!isset($_SESSION['autorise']) ||  $_SESSION['autorise']!="OKAdmin" ){ ?>
        <li class="nav-item">
            <a class="nav-link" href="index.php?uc=produit&action=liste&id_type=1" >Vélo</a>
        </li>
        
        <li class="nav-item">
           <a class="nav-link" href="index.php?uc=produit&action=liste&id_type=2">Accessoire</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="index.php?uc=client&action=formulaire">espace Client</a>
          </li>
     <?php } 
        
        if(isset($_SESSION['autorise']) && $_SESSION['autorise']=="OKClient"){
         
          
      if(isset($_SESSION['badge'])){
        ?><div class='btn btn-danger'><?php
        echo $_SESSION['badge']." produits dans le panier" ; 
      ?></div><?php
    }
      ?>

      <li class="nav-item">
        <a class="nav-link" href="index.php?uc=panier&action=viewPanier"><i class="fas fa-shopping-cart fa-2x" style="color:#FF5733;"></i></a>
      </li>
      <!-- <li class="nav-item">
        <a class="nav-link" href="index.php?uc=produit&action=viderPanier">VIDER LE PANIER</a>
      </li > -->
        <li class="nav-item">
          <a class="nav-link" href="index.php?uc=client&action=deco">Logout</a>
        </li>
    <?php
    } 
        
          if(isset($_SESSION['autorise']) && $_SESSION['autorise']=="OKAdmin"){
            ?>
          <li class="nav-item">
            <a href="index.php?uc=admin&action=espace" class="nav-link">espace Admin</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="index.php?uc=admin&action=deco">Logout</a>
          </li>
      <?php
          }else if(isset($_SESSION['autorise']) && $_SESSION['autorise']=="OKClient"){
              echo "";
          }else{

          

          ?>
          <li class="nav-item">
            <a class="nav-link" href="index.php?uc=admin&action=formulaire">espace Admin</a>
          </li>
        <?php  } ?>
     </ul>
   </div>
</div>
</nav>
<?php } else if($_SESSION['autorise']=="OKClient"){

  include("C:/xampp/htdocs/VELO/view/fragment/navbarClient.php");

   }else if($_SESSION['autorise']=="OKAdmin"){

    include("C:/xampp/htdocs/VELO/view/fragment/navbarAdmin.php");
    
     }
     ?>

    <?php
    //  include("vues/fragments/nav.php"); 
    ?>
    <?= $content ?>

    <footer>
        <div style='padding-top:5%;padding-bottom:5%;' class="container-fluid bg-dark text-light text-center">
            <div class="row">
              <div class="col  "><a class='btn btn-primary'>contactez-nous !</a></div>
              
            </div>
            <div class="row">
                <div class="col col-6"><p>veloclub@gmail.com <br> 01 00 00 00 00 </p></div>
                <div class="col col-6"><img src="https://img.icons8.com/fluency/48/000000/instagram-new.png"/><img src="https://img.icons8.com/color/48/000000/facebook.png"/></div>
            </div>
        </div>
    </footer>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>

    <script type="text/javascript">
        $(document).ready(function() {

            $('.success').delay(1000).slideUp();
        });
    </script>

    </div>

</body>

</html>
